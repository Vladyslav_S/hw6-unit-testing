import React from "react";
import "./CartInput.scss";

const CartInput = (props) => {
  const { type, label, form, field } = props;
  const { name } = field;
  return (
    <div>
      <label>
        {label}:
        {form.errors[name] && form.touched[name] && (
          <div className="error-input">{form.errors[name]}</div>
        )}
        <input type={type} {...field} />
      </label>
    </div>
  );
};

export default CartInput;
