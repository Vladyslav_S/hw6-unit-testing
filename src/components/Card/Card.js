import React from "react";
import "./Card.scss";
import { star } from "../../themes/icons";
import Button from "../Buttom/Button";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { SHOW_MODAL, TOGGLE_FAVORITE } from "../../store/types";

const Card = ({ product, toCart, fromCart }) => {
  const { name, picPath, color, articul, isFavorite } = product;

  const dispatch = useDispatch();

  const toggleFavorite = (id) => {
    dispatch({ type: TOGGLE_FAVORITE, payload: id });
  };

  const showModal = (info, data) => {
    dispatch({ type: SHOW_MODAL, payload: { info, data } });
  };

  return (
    <div className="card-container" style={{ backgroundColor: color }}>
      <img src={picPath} alt="Album face" />
      <h4>{name}</h4>
      <p>Prise: 100500 $</p>
      {toCart && (
        <Button
          className="card-add-button"
          onClick={() => {
            showModal("Add to cart", product);
          }}
          text={"Add to cart"}
        />
      )}
      {fromCart && (
        <Button
          className="card-add-button"
          onClick={() => {
            showModal("Remove from cart", product);
          }}
          text={"X"}
        />
      )}
      <span
        className="card-star"
        onClick={() => {
          toggleFavorite(articul);
        }}
      >
        {star(isFavorite)}
      </span>
    </div>
  );
};

export default Card;

Card.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string,
    picPath: PropTypes.string,
    articul: PropTypes.string,
    color: PropTypes.string,
    isFavorite: PropTypes.bool,
  }),
  toCart: PropTypes.bool,
  fromCart: PropTypes.bool,
};
