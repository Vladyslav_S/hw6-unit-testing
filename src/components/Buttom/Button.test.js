import { render } from "@testing-library/react";
import Button from "./Button";
import userEvent from "@testing-library/user-event";

const id = "ac-button";

describe("Testing Button.js", () => {
  test("Smoke test Button.js", () => {
    render(<Button />);
  });

  test("shows text and have class name", () => {
    const text = "Hello World";
    const className = "black";

    const { getByTestId, getByText } = render(
      <Button text={text} className={className} />
    );

    expect(getByText(/hello world/i)).toBeInTheDocument();
    expect(getByTestId(id)).toHaveClass(className);
  });

  test("function onClick working", () => {
    const onClickFn = jest.fn();

    const { getByTestId } = render(<Button onClick={onClickFn} />);

    userEvent.click(getByTestId(id));

    expect(onClickFn).toHaveBeenCalled();
  });
});
