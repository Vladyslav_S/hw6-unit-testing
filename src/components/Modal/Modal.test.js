import { render } from "@testing-library/react";
import Modal from "./Modal";
import userEvent from "@testing-library/user-event";

jest.mock("react-redux", () => ({
  useSelector: () => {
    return { info: "test action", data: { name: "product name" } };
  },
}));

describe("Testing Modal.js", () => {
  test("Smoke test", () => {
    render(<Modal />);
  });

  test("actions on page", () => {
    const actions = ["Test"];

    render(<Modal actions={actions} />);
  });

  test("check modal closure function", () => {
    const closeModal = jest.fn();
    const id = "modalContainer";

    const { getByTestId } = render(<Modal closeModal={closeModal} />);

    userEvent.click(getByTestId(id));

    expect(closeModal).toHaveBeenCalled();
  });

  test("check modal closure on close button", () => {
    const closeModal = jest.fn();
    const id = "x";

    const { getByTestId } = render(
      <Modal closeModal={closeModal} closeButton />
    );

    userEvent.click(getByTestId(id));

    expect(closeModal).toHaveBeenCalled();
  });
});
