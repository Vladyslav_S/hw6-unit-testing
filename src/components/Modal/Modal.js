import React from "react";
import "./Modal.scss";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

const Modal = (props) => {
  const { closeModal, closeButton, actions } = props;

  const { info: action, data: product } = useSelector((state) => state.modal);

  return (
    <div data-testid="modalContainer" className="modal" onClick={closeModal}>
      <div className="modal-content" onClick={(e) => e.stopPropagation()}>
        <header className="modal-content-header">
          <div>{action}</div>
          {closeButton && (
            <div data-testid="x" onClick={closeModal} className="close">
              &times;
            </div>
          )}
        </header>
        <div className="modal-content-body">
          <p className="modal-content-body-text">
            {`${action} ${product.name}`}
          </p>
          <div>{actions}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;

Modal.propTypes = {
  closeModal: PropTypes.func,
  closeButton: PropTypes.bool,
  actions: PropTypes.array,
};
