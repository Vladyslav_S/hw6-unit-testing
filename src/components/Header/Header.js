import React from "react";

const Header = (props) => {
  return (
    <>
      <h1>Great Shop</h1>
      {props.children}
    </>
  );
};

export default Header;
