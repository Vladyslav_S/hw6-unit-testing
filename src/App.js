import "./App.scss";

import React, { useEffect } from "react";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import Button from "./components/Buttom/Button";
import Sidebar from "./components/Sidebar/Sidebar";
import Routes from "./routes/Routes";
import { loadProducts } from "./store/operations";
import { useDispatch, useSelector } from "react-redux";
import { ADD_TO_CART, CLOSE_MODAL, REMOVE_FROM_CART } from "./store/types";

const App = () => {
  const isLoading = useSelector((state) => state.products.isLoading);
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.modal.isOpen);
  const currentProduct = useSelector((state) => state.modal.data);
  const infoModal = useSelector((state) => state.modal.info);

  const closeModal = () => {
    dispatch({ type: CLOSE_MODAL });
  };

  const addToCart = (data) => {
    dispatch({ type: ADD_TO_CART, payload: data });
  };

  const removeFromCart = (data) => {
    dispatch({ type: REMOVE_FROM_CART, payload: data });
  };

  useEffect(() => {
    dispatch(loadProducts());
  }, [dispatch]);

  if (isLoading) {
    return <div className="App">Loading... Don`t go anywhere</div>;
  }

  return (
    <div className="App">
      <Header>
        <Sidebar />
      </Header>
      <Routes />
      {isOpen && (
        <Modal
          closeButton={true}
          closeModal={closeModal}
          actions={[
            <Button
              key={1}
              onClick={() => {
                if (infoModal === "Add to cart") {
                  addToCart(currentProduct.articul);
                }
                if (infoModal === "Remove from cart") {
                  removeFromCart(currentProduct.articul);
                }
                closeModal();
              }}
              className="modal-button"
              text="Ok"
            />,
            <Button
              key={2}
              onClick={closeModal}
              className="modal-button"
              text="Cancel"
            />,
          ]}
        />
      )}
    </div>
  );
};

export default App;
