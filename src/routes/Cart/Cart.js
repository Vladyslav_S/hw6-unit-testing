import React from "react";
import "./Cart.scss";
import Card from "../../components/Card/Card";
import { useSelector } from "react-redux";
import CartForm from "../../components/CartForm/CartForm";

const Cart = () => {
  const products = useSelector((state) => state.products.data);
  const cart = products.filter((product) => product.inCartAmount);

  if (cart.length === 0) {
    return <div className="container">Cart is empty</div>;
  }

  return (
    <>
      <div className="container">
        {cart.map((product) => {
          return <Card key={product.articul} product={product} fromCart />;
        })}
      </div>
      <CartForm />
    </>
  );
};

export default Cart;
