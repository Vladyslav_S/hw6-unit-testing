import React from "react";
import Card from "../../components/Card/Card";
import { useSelector } from "react-redux";

function Favorites() {
  const products = useSelector((state) => state.products.data);
  const favorites = products.filter((product) => product.isFavorite);

  if (!favorites.length) {
    return <div>Favorites are empty</div>;
  }

  return (
    <div className="container">
      {favorites.map((product) => {
        return <Card key={product.articul} product={product} toCart />;
      })}
    </div>
  );
}

export default Favorites;
